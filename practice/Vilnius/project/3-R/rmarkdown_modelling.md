---
title: "test"
author: "test"
date: "10/3/2021"
output:
  html_document:
    keep_md: true
---



## h2o gridsearch


```r
library(h2o)
```

```
## 
## ----------------------------------------------------------------------
## 
## Your next step is to start H2O:
##     > h2o.init()
## 
## For H2O package documentation, ask for help:
##     > ??h2o
## 
## After starting H2O, you can use the Web UI at http://localhost:54321
## For more information visit https://docs.h2o.ai
## 
## ----------------------------------------------------------------------
```

```
## 
## Pridedamas paketas: 'h2o'
```

```
## Šie objektai yra užmaskuoti nuo 'package:stats':
## 
##     cor, sd, var
```

```
## Šie objektai yra užmaskuoti nuo 'package:base':
## 
##     %*%, %in%, &&, ||, apply, as.factor, as.numeric, colnames,
##     colnames<-, ifelse, is.character, is.factor, is.numeric, log,
##     log10, log1p, log2, round, signif, trunc
```

```r
h2o.init()
```

```
## 
## H2O is not running yet, starting it now...
## 
## Note:  In case of errors look at the following log files:
##     C:\Users\kesdau\AppData\Local\Temp\RtmpYRc6Bz\file222450134e13/h2o_kesdau_started_from_r.out
##     C:\Users\kesdau\AppData\Local\Temp\RtmpYRc6Bz\file222456db3c9c/h2o_kesdau_started_from_r.err
## 
## 
## Starting H2O JVM and connecting:  Connection successful!
## 
## R is connected to the H2O cluster: 
##     H2O cluster uptime:         2 seconds 
##     H2O cluster timezone:       Europe/Helsinki 
##     H2O data parsing timezone:  UTC 
##     H2O cluster version:        3.34.0.3 
##     H2O cluster version age:    1 month and 13 days  
##     H2O cluster name:           H2O_started_from_R_kesdau_mkh685 
##     H2O cluster total nodes:    1 
##     H2O cluster total memory:   3.52 GB 
##     H2O cluster total cores:    4 
##     H2O cluster allowed cores:  4 
##     H2O cluster healthy:        TRUE 
##     H2O Connection ip:          localhost 
##     H2O Connection port:        54321 
##     H2O Connection proxy:       NA 
##     H2O Internal Security:      FALSE 
##     H2O API Extensions:         Amazon S3, Algos, AutoML, Core V3, TargetEncoder, Core V4 
##     R Version:                  R version 4.1.1 (2021-08-10)
```



```r
df <- h2o.importFile("../1-data/1-sample_data.csv")
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```

```r
head(df)
```

```
##   id y amount_current_loan  term credit_score       loan_purpose yearly_income
## 1  1 0              386342 short    very_good              other       1371971
## 2  2 0              429000 short         good debt_consolidation        823042
## 3  3 0              344608 short         good debt_consolidation       1316567
## 4  4 0               71214 short           NA debt_consolidation           NaN
## 5  5 0              654126 short         good debt_consolidation       1242847
## 6  6 0              133034 short         good debt_consolidation        651035
##   home_ownership bankruptcies
## 1           rent            0
## 2       mortgage            0
## 3           rent            0
## 4           rent            0
## 5           rent            0
## 6           rent            0
```

Name X and Y


```r
y <- "y"
x <- setdiff(names(df), c(y, "id"))
df$y <- as.factor(df$y)
```

Gridsearch params


```r
dl_params <- list(hidden = list(10, c(10, 10), c(10,10,10)))
```


```r
dl_grid <- h2o.grid(algorithm = "deeplearning",
                    grid_id = "ktu_grid",
                    x = x,
                    y = y,
                    df,
                    epochs = 1,
                    hyper_params = dl_params)
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```


```r
h2o.getGrid(dl_grid@grid_id,
            sort_by = "auc")
```

```
## H2O Grid Details
## ================
## 
## Grid ID: ktu_grid 
## Used hyper parameters: 
##   -  hidden 
## Number of models: 3 
## Number of failed models: 0 
## 
## Hyper-Parameter Search Summary: ordered by increasing auc
##         hidden        model_ids     auc
## 1           10 ktu_grid_model_1 0.64792
## 2     [10, 10] ktu_grid_model_2 0.65445
## 3 [10, 10, 10] ktu_grid_model_3 0.67606
```


```r
best_grid <- h2o.getModel(dl_grid@model_ids[[3]])
```



```r
h2o.saveModel(best_grid, "../4-model/grid")
```

```
## [1] "C:\\KTU\\dvda-project\\practice\\Vilnius\\project\\4-model\\grid\\ktu_grid_model_3"
```



```r
test <- h2o.importFile("../1-data/test_data.csv")
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```


```r
p <- h2o.predict(best_grid, test)
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```


```r
p
```

```
##   predict          p0           p1
## 1       1 0.999974116 2.588444e-05
## 2       1 0.003307435 9.966926e-01
## 3       0 0.999993113 6.886772e-06
## 4       1 0.999979862 2.013845e-05
## 5       1 0.999922998 7.700153e-05
## 6       1 0.999987646 1.235443e-05
## 
## [10000 rows x 3 columns]
```


```r
library(tidyverse)
```

```
## -- Attaching packages --------------------------------------- tidyverse 1.3.1 --
```

```
## v ggplot2 3.3.5     v purrr   0.3.4
## v tibble  3.1.6     v dplyr   1.0.7
## v tidyr   1.1.4     v stringr 1.4.0
## v readr   2.1.0     v forcats 0.5.1
```

```
## Warning: paketas 'tibble' buvo sukurtas pagal R versiją 4.1.2
```

```
## Warning: paketas 'readr' buvo sukurtas pagal R versiją 4.1.2
```

```
## -- Conflicts ------------------------------------------ tidyverse_conflicts() --
## x dplyr::filter() masks stats::filter()
## x dplyr::lag()    masks stats::lag()
```

```r
p_r <- p %>% 
  as_tibble()

p_r %>%
  mutate(y = p1,
         id = row_number()) %>%
  select(id, y) %>%
  write_csv("../5-predictions/predict.csv")
```

