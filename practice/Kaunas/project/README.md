### DVDA Projektas

Geriausias modelis gautas naudojant [Distributed Random Forest](https://docs.h2o.ai/h2o/latest-stable/h2o-docs/data-science/drf.html).

Modelio paieškai buvo pasitelktas AutoML su parametrais:

* max_runtime_secs = 3600
* nfolds = 3
* ...

Modelis optimizuotas naudojant gridsearch algoritmą. Pasirinkti parametrai:

* ntrees = 50, 100, 200
* max_depth = 10, 20, 30
* ...

Geriausias modelis išsaugotas **4-model/DRF_model_R_1637942391682_22**, jo hyperparametrai:


```R
> model <- h2o.loadModel("4-model/DRF_model_R_1637942391682_22")
> model@allparameters
$model_id
[1] "DRF_model_R_1637942391682_22"

$nfolds
[1] 10

$keep_cross_validation_models
[1] TRUE

$keep_cross_validation_predictions
[1] FALSE

$keep_cross_validation_fold_assignment
[1] FALSE

$score_each_iteration
[1] FALSE

$score_tree_interval
[1] 0

$fold_assignment
[1] "Stratified"

$ignore_const_cols
[1] TRUE

$balance_classes
[1] FALSE

$max_after_balance_size
[1] 5

$max_confusion_matrix_size
[1] 20

$ntrees
[1] 50

$max_depth
[1] 20

$min_rows
[1] 1

$nbins
[1] 20

$nbins_top_level
[1] 1024

$nbins_cats
[1] 1024

$r2_stopping
[1] 1.797693e+308

$stopping_rounds
[1] 0

$stopping_tolerance
[1] 0.001

$max_runtime_secs
[1] 0

$seed
[1] 1234

$build_tree_one_node
[1] FALSE

$mtries
[1] -1

$sample_rate
[1] 0.632

$binomial_double_trees
[1] FALSE

$col_sample_rate_change_per_level
[1] 1

$col_sample_rate_per_tree
[1] 1

$min_split_improvement
[1] 1e-05

$histogram_type
[1] "UniformAdaptive"

$categorical_encoding
[1] "OneHotExplicit"

$calibrate_model
[1] FALSE

$distribution
[1] "multinomial"

$check_constant_response
[1] TRUE

$gainslift_bins
[1] -1

$auc_type
[1] "AUTO"

$x
 [1] "amount_current_loan"          "term"                         "credit_score"                 "loan_purpose"                
 [5] "yearly_income"                "home_ownership"               "bankruptcies"                 "years_current_job"           
 [9] "monthly_debt"                 "years_credit_history"         "months_since_last_delinquent" "open_accounts"               
[13] "credit_problems"              "credit_balance"               "max_open_credit"             

$y
[1] "y"
```

Gauta **AUC** metrika:

* training 0.8
* validation 0.75
* test 0.7

Treniravimo kodas išsaugotas **3-R/jusu_R_arba_Rmd_failas.R**

Aprašyti modifikuotus parametrus, pvz:

* Stratified fold_assignment buvo pasirinktas dėl netolygaus y pasiskirstymo
* ntrees buvo padidintas iki xxx
* ...


Geriausias modelis naudojamas shiny web aplikacijai:

![Shiny app](app/shiny.png)
