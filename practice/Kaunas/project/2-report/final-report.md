---
title: "Exploratory Analysis"
author: "KD"
date: "12/10/2021"
output:
  html_document:
    keep_md: true
---

Užkrauname reikalingas bibliotekas


```r
library(tidyverse)
library(knitr)
```

Generuojant ataskaitą galima failo neskaityti kiekvieną kartą iš naujo - cache=TRUE. Nenorint klaidų/informacinių pranešimų pridedame message=FALSE ir warning=FALSE.


```r
df <- read_csv("../1-data/5pct_sample.csv")
```

Duomenų failo dimensijos:



```r
dim(df)
```

```
## [1] 500000     17
```
# Kintamųjų apžvalga


(dėl gražesnio spaudinimo, naudojame funkciją kable() ir išdaliname kintamuosius į kelias eilutes)



```r
summary(df[1:6]) %>%
  kable()
```



|   |      id        |      y        |amount_current_loan |    term         |credit_score     |loan_purpose     |
|:--|:---------------|:--------------|:-------------------|:----------------|:----------------|:----------------|
|   |Min.   :     12 |Min.   :0.0000 |Min.   :   10802    |Length:500000    |Length:500000    |Length:500000    |
|   |1st Qu.:2497598 |1st Qu.:0.0000 |1st Qu.:  179696    |Class :character |Class :character |Class :character |
|   |Median :4995806 |Median :0.0000 |Median :  312576    |Mode  :character |Mode  :character |Mode  :character |
|   |Mean   :4995926 |Mean   :0.2429 |Mean   :11753811    |NA               |NA               |NA               |
|   |3rd Qu.:7488490 |3rd Qu.:0.0000 |3rd Qu.:  524942    |NA               |NA               |NA               |
|   |Max.   :9999985 |Max.   :1.0000 |Max.   :99999999    |NA               |NA               |NA               |



```r
summary(df[7:13]) %>%
  kable()
```



|   |yearly_income     |home_ownership   | bankruptcies  |years_current_job | monthly_debt  |years_credit_history |months_since_last_delinquent |
|:--|:-----------------|:----------------|:--------------|:-----------------|:--------------|:--------------------|:----------------------------|
|   |Min.   :    76627 |Length:500000    |Min.   :0.0000 |Min.   : 0.000    |Min.   :     0 |Min.   : 4.00        |Min.   :  0.00               |
|   |1st Qu.:   849623 |Class :character |1st Qu.:0.0000 |1st Qu.: 3.000    |1st Qu.: 10222 |1st Qu.:14.00        |1st Qu.: 16.00               |
|   |Median :  1174352 |Mode  :character |Median :0.0000 |Median : 6.000    |Median : 16250 |Median :17.00        |Median : 32.00               |
|   |Mean   :  1379892 |NA               |Mean   :0.1174 |Mean   : 5.885    |Mean   : 18484 |Mean   :18.21        |Mean   : 35.03               |
|   |3rd Qu.:  1652316 |NA               |3rd Qu.:0.0000 |3rd Qu.:10.000    |3rd Qu.: 24024 |3rd Qu.:22.00        |3rd Qu.: 51.00               |
|   |Max.   :165557393 |NA               |Max.   :7.0000 |Max.   :10.000    |Max.   :435843 |Max.   :70.00        |Max.   :176.00               |
|   |NA's   :95755     |NA               |NA's   :856    |NA's   :21136     |NA             |NA                   |NA's   :265982               |

Galutinėje ataskaitoje galime neįtraukti R kodo, naudojant echo=FALSE parametrą. 


|   |open_accounts |credit_problems |credit_balance   |max_open_credit   |
|:--|:-------------|:---------------|:----------------|:-----------------|
|   |Min.   : 0.00 |Min.   : 0.0000 |Min.   :       0 |Min.   :0.000e+00 |
|   |1st Qu.: 8.00 |1st Qu.: 0.0000 |1st Qu.:  113202 |1st Qu.:2.768e+05 |
|   |Median :10.00 |Median : 0.0000 |Median :  210653 |Median :4.721e+05 |
|   |Mean   :11.16 |Mean   : 0.1735 |Mean   :  296789 |Mean   :7.666e+05 |
|   |3rd Qu.:14.00 |3rd Qu.: 0.0000 |3rd Qu.:  370804 |3rd Qu.:7.916e+05 |
|   |Max.   :76.00 |Max.   :15.0000 |Max.   :32878968 |Max.   :1.540e+09 |
|   |NA            |NA              |NA               |NA's   :8         |


# TO DO

Apžvelgti NA reikšmes, y pasiskirstymą, character tipo kintamuosius panagrinėti detaliau.



```r
df$loan_purpose <- as.factor(df$loan_purpose)
df$y <- as.factor(df$y)
```



```r
summary(df$loan_purpose) %>%
  kable()
```



|                     |      x|
|:--------------------|------:|
|buy_a_car            |   6401|
|buy_house            |   3329|
|business_loan        |   7816|
|debt_consolidation   | 393322|
|educational_expenses |    531|
|home_improvements    |  29178|
|major_purchase       |   1805|
|medical_bills        |   5482|
|moving               |    760|
|other                |  46062|
|renewable_energy     |     53|
|small_business       |   1374|
|take_a_trip          |   2765|
|vacation             |    549|
|wedding              |    573|


Arba:


```r
df %>%
  group_by(loan_purpose) %>%
  summarise(n = n()) %>%
  arrange(desc(n)) %>%
  kable()
```



|loan_purpose         |      n|
|:--------------------|------:|
|debt_consolidation   | 393322|
|other                |  46062|
|home_improvements    |  29178|
|business_loan        |   7816|
|buy_a_car            |   6401|
|medical_bills        |   5482|
|buy_house            |   3329|
|take_a_trip          |   2765|
|major_purchase       |   1805|
|small_business       |   1374|
|moving               |    760|
|wedding              |    573|
|vacation             |    549|
|educational_expenses |    531|
|renewable_energy     |     53|

Pasirinkus kintamuosius juos vizualizuokite


```r
df %>%
  group_by(y, loan_purpose) %>%
  summarise(n = n()) %>%
  ggplot(aes(fill=y, y=n, x=loan_purpose)) + 
    geom_bar(position="dodge", stat="identity") + 
    coord_flip() +
    scale_y_continuous(labels = scales::comma) +
    theme_dark()
```

![](final-report_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

Daugiausiai banktotų imant paskolą šiems tikslams:


```r
df %>%
  filter(y == 1) %>%
  group_by(loan_purpose) %>%
  summarise(n = n()) %>%
  arrange(desc(n)) %>%
  head(10) %>%
  kable()
```



|loan_purpose       |     n|
|:------------------|-----:|
|debt_consolidation | 95219|
|other              | 10973|
|home_improvements  |  6473|
|business_loan      |  2547|
|medical_bills      |  1497|
|buy_a_car          |  1374|
|buy_house          |   914|
|take_a_trip        |   706|
|small_business     |   531|
|major_purchase     |   530|


# Papildomi pasiūlymai interaktyvumui pagerinti

Interaktyvios lentelės su datatable (DT)


```r
library(DT)
df %>%
  group_by(y, loan_purpose) %>%
  summarise(n = n()) %>%
  datatable()
```

```{=html}
<div id="htmlwidget-79d4ffdf0cb66d1866cb" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-79d4ffdf0cb66d1866cb">{"x":{"filter":"none","vertical":false,"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"],["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","1","1","1","1","1","1","1","1","1","1","1","1","1","1","1"],["buy_a_car","buy_house","business_loan","debt_consolidation","educational_expenses","home_improvements","major_purchase","medical_bills","moving","other","renewable_energy","small_business","take_a_trip","vacation","wedding","buy_a_car","buy_house","business_loan","debt_consolidation","educational_expenses","home_improvements","major_purchase","medical_bills","moving","other","renewable_energy","small_business","take_a_trip","vacation","wedding"],[5027,2415,5269,298103,389,22705,1275,3985,543,35089,34,843,2059,407,405,1374,914,2547,95219,142,6473,530,1497,217,10973,19,531,706,142,168]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>y<\/th>\n      <th>loan_purpose<\/th>\n      <th>n<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":3},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script>
```

Interaktyvūs grafikai su plotly


```r
library(plotly)
df %>%
  group_by(y, credit_score) %>%
  summarise(n = n()) %>%
  plot_ly(x = ~credit_score, y = ~n, name = ~y, type = "bar")
```

```{=html}
<div id="htmlwidget-6115531f2648f9a61044" style="width:672px;height:480px;" class="plotly html-widget"></div>
<script type="application/json" data-for="htmlwidget-6115531f2648f9a61044">{"x":{"visdat":{"4920285947f9":["function () ","plotlyVisDat"]},"cur_data":"4920285947f9","attrs":{"4920285947f9":{"x":{},"y":{},"name":{},"alpha_stroke":1,"sizes":[10,100],"spans":[1,20],"type":"bar"}},"layout":{"margin":{"b":40,"l":60,"t":25,"r":10},"xaxis":{"domain":[0,1],"automargin":true,"title":"credit_score","type":"category","categoryorder":"array","categoryarray":["fair","good","very_good"]},"yaxis":{"domain":[0,1],"automargin":true,"title":"n"},"hovermode":"closest","showlegend":true},"source":"A","config":{"modeBarButtonsToAdd":["hoverclosest","hovercompare"],"showSendToCloud":false},"data":[{"x":["fair","good","very_good"],"y":[19285,212130,78885],"name":"0","type":"bar","marker":{"color":"rgba(31,119,180,1)","line":{"color":"rgba(31,119,180,1)"}},"error_y":{"color":"rgba(31,119,180,1)"},"error_x":{"color":"rgba(31,119,180,1)"},"xaxis":"x","yaxis":"y","frame":null},{"x":["fair","good","very_good"],"y":[8140,52625,10236],"name":"1","type":"bar","marker":{"color":"rgba(255,127,14,1)","line":{"color":"rgba(255,127,14,1)"}},"error_y":{"color":"rgba(255,127,14,1)"},"error_x":{"color":"rgba(255,127,14,1)"},"xaxis":"x","yaxis":"y","frame":null}],"highlight":{"on":"plotly_click","persistent":false,"dynamic":false,"selectize":false,"opacityDim":0.2,"selected":{"opacity":1},"debounce":0},"shinyEvents":["plotly_hover","plotly_click","plotly_selected","plotly_relayout","plotly_brushed","plotly_brushing","plotly_clickannotation","plotly_doubleclick","plotly_deselect","plotly_afterplot","plotly_sunburstclick"],"base_url":"https://plot.ly"},"evals":[],"jsHooks":[]}</script>
```


