---
title: "test"
author: "test"
date: "10/3/2021"
output:
  html_document:
    keep_md: true
---


```r
library(h2o)
```

```
## 
## ----------------------------------------------------------------------
## 
## Your next step is to start H2O:
##     > h2o.init()
## 
## For H2O package documentation, ask for help:
##     > ??h2o
## 
## After starting H2O, you can use the Web UI at http://localhost:54321
## For more information visit https://docs.h2o.ai
## 
## ----------------------------------------------------------------------
```

```
## 
## Attaching package: 'h2o'
```

```
## The following objects are masked from 'package:stats':
## 
##     cor, sd, var
```

```
## The following objects are masked from 'package:base':
## 
##     &&, %*%, %in%, ||, apply, as.factor, as.numeric, colnames,
##     colnames<-, ifelse, is.character, is.factor, is.numeric, log,
##     log10, log1p, log2, round, signif, trunc
```

```r
h2o.init()
```

```
##  Connection successful!
## 
## R is connected to the H2O cluster: 
##     H2O cluster uptime:         42 minutes 6 seconds 
##     H2O cluster timezone:       Europe/Vilnius 
##     H2O data parsing timezone:  UTC 
##     H2O cluster version:        3.32.1.3 
##     H2O cluster version age:    5 months and 24 days !!! 
##     H2O cluster name:           H2O_started_from_R_kestutis_oes149 
##     H2O cluster total nodes:    1 
##     H2O cluster total memory:   3.92 GB 
##     H2O cluster total cores:    8 
##     H2O cluster allowed cores:  8 
##     H2O cluster healthy:        TRUE 
##     H2O Connection ip:          localhost 
##     H2O Connection port:        54321 
##     H2O Connection proxy:       NA 
##     H2O Internal Security:      FALSE 
##     H2O API Extensions:         Amazon S3, XGBoost, Algos, AutoML, Core V3, TargetEncoder, Core V4 
##     R Version:                  R version 4.1.1 (2021-08-10)
```

```
## Warning in h2o.clusterInfo(): 
## Your H2O cluster version is too old (5 months and 24 days)!
## Please download and install the latest version from http://h2o.ai/download/
```



```r
df <- h2o.importFile("../1-data/1-sample_data.csv")
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```

```r
y <- "y"
x <- setdiff(names(df), c(y, "id"))
df$y <- as.factor(df$y)
```


```r
dl_model <- h2o.deeplearning(x,
                             y,
                             df,
                             hidden = c(10, 10))
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |===                                                                   |   4%
  |                                                                            
  |========                                                              |  11%
  |                                                                            
  |===============                                                       |  21%
  |                                                                            
  |====================                                                  |  29%
  |                                                                            
  |===========================                                           |  38%
  |                                                                            
  |================================                                      |  45%
  |                                                                            
  |======================================                                |  55%
  |                                                                            
  |=============================================                         |  64%
  |                                                                            
  |====================================================                  |  74%
  |                                                                            
  |===========================================================           |  84%
  |                                                                            
  |=================================================================     |  93%
  |                                                                            
  |======================================================================| 100%
```

```r
dl_model
```

```
## Model Details:
## ==============
## 
## H2OBinomialModel: deeplearning
## Model ID:  DeepLearning_model_R_1636734668821_34 
## Status of Neuron Layers: predicting y, 2-class classification, bernoulli distribution, CrossEntropy loss, 452 weights/biases, 11.3 KB, 10,096,572 training samples, mini-batch size 1
##   layer units      type dropout       l1       l2 mean_rate rate_rms momentum
## 1     1    31     Input  0.00 %       NA       NA        NA       NA       NA
## 2     2    10 Rectifier  0.00 % 0.000000 0.000000  0.132925 0.342721 0.000000
## 3     3    10 Rectifier  0.00 % 0.000000 0.000000  0.008405 0.021478 0.000000
## 4     4     2   Softmax      NA 0.000000 0.000000  0.002003 0.001700 0.000000
##   mean_weight weight_rms mean_bias bias_rms
## 1          NA         NA        NA       NA
## 2   -0.006942   0.261051  0.123827 0.368879
## 3   -0.000796   0.318391  0.895327 0.253253
## 4   -0.479045   1.653410 -0.031980 0.015677
## 
## 
## H2OBinomialMetrics: deeplearning
## ** Reported on training data. **
## ** Metrics reported on temporary training frame with 9952 samples **
## 
## MSE:  0.3682447
## RMSE:  0.6068317
## LogLoss:  1.124432
## Mean Per-Class Error:  0.4401879
## AUC:  0.6728384
## AUCPR:  0.6975613
## Gini:  0.3456768
## 
## Confusion Matrix (vertical: actual; across: predicted) for F1-optimal threshold:
##           0    1    Error        Rate
## 0      1018 3971 0.795951  =3971/4989
## 1       419 4544 0.084425   =419/4963
## Totals 1437 8515 0.441117  =4390/9952
## 
## Maximum Metrics: Maximum metrics at their respective thresholds
##                         metric threshold       value idx
## 1                       max f1  0.045713    0.674284 363
## 2                       max f2  0.004957    0.832606 399
## 3                 max f0point5  0.088435    0.618663 300
## 4                 max accuracy  0.091868    0.619875 295
## 5                max precision  0.997851    1.000000   0
## 6                   max recall  0.004957    1.000000 399
## 7              max specificity  0.997851    1.000000   0
## 8             max absolute_mcc  0.147935    0.262144 231
## 9   max min_per_class_accuracy  0.085669    0.615354 303
## 10 max mean_per_class_accuracy  0.091868    0.619792 295
## 11                     max tns  0.997851 4989.000000   0
## 12                     max fns  0.997851 4961.000000   0
## 13                     max fps  0.007980 4989.000000 398
## 14                     max tps  0.004957 4963.000000 399
## 15                     max tnr  0.997851    1.000000   0
## 16                     max fnr  0.997851    0.999597   0
## 17                     max fpr  0.007980    1.000000 398
## 18                     max tpr  0.004957    1.000000 399
## 
## Gains/Lift Table: Extract with `h2o.gainsLift(<model>, <data>)` or `h2o.gainsLift(<model>, valid=<T/F>, xval=<T/F>)`
```


```r
dl_grid <- list(hidden = list(10, 20, c(5, 5)))

dl_grids <- h2o.grid("deeplearning",
                     grid_id  = dl_grid,
                     x = x,
                     y = y,
                     training_frame = df,
                     epochs = 1,
                     hyper_params = dl_grid)
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```


```r
h2o.getGrid(grid_id = dl_grids@grid_id,
            sort_by = "auc")
```

```
## H2O Grid Details
## ================
## 
## Grid ID: list(hidden = list(10, 20, c(5, 5))) 
## Used hyper parameters: 
##   -  hidden 
## Number of models: 6 
## Number of failed models: 0 
## 
## Hyper-Parameter Search Summary: ordered by increasing auc
##   hidden                                    model_ids                auc
## 1   [10] list(hidden = list(10, 20, c(5, 5)))_model_4 0.6334101421006186
## 2   [20] list(hidden = list(10, 20, c(5, 5)))_model_5 0.6459411263206949
## 3   [10] list(hidden = list(10, 20, c(5, 5)))_model_1 0.6696797598262563
## 4   [20] list(hidden = list(10, 20, c(5, 5)))_model_2 0.6729208829717128
## 5 [5, 5] list(hidden = list(10, 20, c(5, 5)))_model_3 0.6732146233889928
## 6 [5, 5] list(hidden = list(10, 20, c(5, 5)))_model_6 0.6888624751463749
```



```r
df_test <- h2o.importFile("../1-data/test_data.csv")
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```

```r
p <- h2o.predict(dl_model, df_test)
```

```
## 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |======================================================================| 100%
```




```r
library(tidyverse)
```

```
## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.1 ──
```

```
## ✓ ggplot2 3.3.5     ✓ purrr   0.3.4
## ✓ tibble  3.1.4     ✓ dplyr   1.0.7
## ✓ tidyr   1.1.3     ✓ stringr 1.4.0
## ✓ readr   2.0.1     ✓ forcats 0.5.1
```

```
## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
## x dplyr::filter() masks stats::filter()
## x dplyr::lag()    masks stats::lag()
```

```r
p_R <- p %>%
  as_tibble() %>%
  mutate(y = p1) %>%
  select(y) %>%
  rownames_to_column("id")

write_csv(p_R, "../5-predictions/predict.csv")
```
