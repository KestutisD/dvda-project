---
title: "test"
author: "test"
date: "10/3/2021"
output:
  html_document:
    keep_md: true
---

```{r }
library(h2o)
h2o.init()
```


```{r }
df <- h2o.importFile("../1-data/1-sample_data.csv")
y <- "y"
x <- setdiff(names(df), c(y, "id"))
df$y <- as.factor(df$y)
```

```{r}
dl_model <- h2o.deeplearning(x,
                             y,
                             df,
                             hidden = c(10, 10))

dl_model
```

```{r }
dl_grid <- list(hidden = list(10, 20, c(5, 5)))

dl_grids <- h2o.grid("deeplearning",
                     grid_id  = dl_grid,
                     x = x,
                     y = y,
                     training_frame = df,
                     epochs = 1,
                     hyper_params = dl_grid)


```

```{r }
h2o.getGrid(grid_id = dl_grids@grid_id,
            sort_by = "auc")


```


```{r }

df_test <- h2o.importFile("../1-data/test_data.csv")
p <- h2o.predict(dl_model, df_test)


```



```{r }
library(tidyverse)

p_R <- p %>%
  as_tibble() %>%
  mutate(y = p1) %>%
  select(y) %>%
  rownames_to_column("id")

write_csv(p_R, "../5-predictions/predict.csv")

```

# Test

* ds


```{r}
h2o.saveModel(dl_model, "../4-model/")


```


```{r}
dl_model2 <- h2o.loadModel("../4-model/DeepLearning_model_R_1636734668821_6")

p <- h2o.predict(dl_model2, df_test)
h2o.auc(dl_model2)
```

