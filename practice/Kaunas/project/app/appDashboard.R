library(shinydashboard)
library(shiny)
ui <- dashboardPage(
  dashboardHeader(title = "Loan dashboard"),
  dashboardSidebar(fileInput("file", "Ikelti csv faila")),
  dashboardBody(
    # valueBox(100, "Basic example"),
    tableOutput("table"),
    dataTableOutput("predictions")
  )
)
server <- function(input, output) {
  h2o.init()
  model <- h2o.loadModel("../4-model/DRF_model_R_1637942391682_22")
  output$table <- renderTable({
    req(input$file)
    table <- read_csv(input$file$datapath) %>%
      group_by(credit_score) %>%
      summarise(n = n())
    table
  })
  
  output$predictions <- renderDataTable({
    req(input$file)
    df_test <- h2o.importFile(input$file$datapath)
    p <- h2o.predict(model, df_test)
    p %>%
      as_tibble() %>%
      mutate(y = predict) %>%
      select(y) %>%
      rownames_to_column("id") %>%
      head(20)
  })
}
shinyApp(ui, server)